# Unopiniated script that let's you emulate a file manager 
## Features :

- Configurable : Set your own default applications.

- Extensible : You can create your own extensions for the menu.

- Shortcuts : Create shortcuts for whatever you want.

- Search for files in directory hierarchy (requires `fd`)*

- Uwu mode: You're not mistaken, uwu mode is finally implemented. I know you've been waiting for this. Just launch the script with the runner and the `--uwu` flag.

The script requires a runner aka something like rofi, dmenu or wofi that can reads from stdin and displays options which when selected will be output to stdout (man 7 wofi).

### How do I set this up?

1. `git clone https://gitlab.com/snakedye/wofer`

2. `cd wofer; wofer [command]`

If you're an Arch user it's on the AUR under the name of `wofer-git`.

### Basic shortcuts

- `:help` help

- `:h` show/hide hidden files

- `? or :` menu for directory

- `!delete` delete current directory (doesn't force by default)

- `:m` manga extension 

- `?pattern` search for pattern in directory hierarchy*

### Menu options

Menu options are stored in the `prompt` function under this format:

```bash
if [ -d "$1" ]; then # Options for directories
   blah..
   echo "icon option_name $PWD"
else # Options for files
   echo "icon option_name $2"
fi
```

Options for files are under the else statement and entries for directories below the if statement

Commands associated with menu options are stored in the `menu` function.

```bash
menu () {
    blah...
    blah...
    blah..
    case $(echo "$OPTIONS" | sed 's/ .*//') in
        option_name)
            command
            ;;
        ...
    esac
```

#### Steps:

1. Add something along those line : `echo "icon  operation_name $1"` in `prompt`

2. Add a matching case in `menu` using the template shown.

### Custom shortcuts

To create a shortcut for a directory use the following template inside the 
`shortcut` function:

```bash
fbiopenup)
    command
    ;;
```

Path shortcut can be inserted blow  the `*)` case

```bash
elif [[ "$1" =~ ^shortcut ]]; then
    str='shortcut'
    cd "${1/$str/"/path/to/whatever"}"
```

It's also possible to link external scripts like the manga extentions.

#### Launcher

Within the case statement add:

```bash
filetype)
    command "$1"
    ;;
```
### Configuration

Wofer will look for a config in `~/.config/wofer/config`. You can use other config files with `--c` argument.

Options :

- `--no-reload | NO_RELOAD=true|false` choose if the script runs once of loops

- `SHOW_HIDDEN=true|false` show/hide hidden files

- `EXEC=runner` the default runner, I recommend using a script if your command requires more than a word.

- `TERM=terminal` this overrides the default terminal of your shell

- `UWU=true|false` enable/disable uwu mode

- `EXTENSIONS=/path/to/directory/` directory where extensions are stored

- `FM=fm` external file manager could also be set to you shell

### TO-DOs

- [x] KDE Connect integration

- [x] GUI for copying and moving files/directories

- [ ] Compress to .. (Not coming anytime soon)

- [x] Package it on the AUR
